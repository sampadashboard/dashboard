class DevNotifierMailer < ApplicationMailer
  default :from => 'sampa@dash.com'
  def dev_invite (tk, link)
      @tok = tk
      @link = link
      mail(:to => @tok,
      :subject => '[SAMPA] Voce foi convidado para ser desenvolvedor' )
  end
end
