class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.from_omniauth(request.env["omniauth.auth"])
    log_in(user)
    redirect_to root_path

  end

  def create2
    user = User.find_by(email: params[:session][:email].downcase,
                        password: params[:session][:password])

    respond_to do |format|
      if user
        log_in user
        format.html {redirect_to root_path, notice: 'You logged in.'}
        format.json {head :no_content}
      else
        format.html {redirect_to login_path, notice: "Invalid email/password combination"}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end

    end

  end

  def destroy
    session[:user_id] = session[:user_dashboard_id] = session[:app_id] = session[:dev_team_id] = nil
    respond_to do |format|
      format.html {redirect_to root_path, notice: 'You logged out.'}
      format.json {head :no_content}
    end
  end
end
