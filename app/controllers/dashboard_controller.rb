require 'net/http'
require 'uri'

class DashboardController < ApplicationController

  before_action :authenticate_user
  before_action :define_dashboard
  before_action :set_session
  before_action :verify_dashboard_owner, only: [:load_user_dashboard, :delete_window]

  def dashboard
    @dashboards = current_user.user_dashboards.order(:id)
    verify_if_updated @dashboards
  end

  def load_user_dashboard
    begin
      windows = Window.where user_dashboard_id: current_dashboard.id
      render json: windows, include: :app
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  def create_user_dashboard
    begin
      dashboard = UserDashboard.new user_id: current_user.id, description: params[:name]
      dashboard.save!
      render json: dashboard
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  def delete_dashboard
    begin
      dashboard_id = params[:dashid]
      windows = Window.where(user_dashboard_id: dashboard_id)
      windows.each do |w|
        w.delete
      end
      UserDashboard.find(dashboard_id).delete
      render json: true
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  private

  def set_session
    session[:user_dashboard_id] = params[:dashid] if params[:dashid]
    session[:app_id] = params[:appid] if params[:appid]
  end

  def define_dashboard
    if not current_dashboard
      if current_user.user_dashboards.empty?
        dashboard = current_user.user_dashboards.create(description: 'Default')
        session[:user_dashboard_id] = dashboard.id
      elsif not current_dashboard
        dashboard = current_user.user_dashboards.first
        session[:user_dashboard_id] = dashboard.id
      end
    end
  end

  def verify_if_updated(dashboards)
    dashboards.each do |dashboard|
      windows = dashboard.windows
      windows.each do |window|
        if not updated?(window)
          flash[:notice] = 'Your window is outdated. Please update it.'
          redirect_to edit_window_path(id: window.id, app_id: window.app.id) and return
        end
      end
    end
  end

  def updated?(window)
    eval(window.app.version) == eval(window.app_version)
  end
end
