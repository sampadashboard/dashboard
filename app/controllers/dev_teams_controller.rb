class DevTeamsController < ApplicationController
  before_action :set_dev_team, only: [:show, :edit, :update, :destroy, :invite_to_dev_team, :invite]

  # GET /dev_teams
  # GET /dev_teams.json
  def index
     #@dev_teams = DevTeam.all
    @dev_teams = current_user.dev_teams
  end

  # GET /dev_teams/1
  # GET /dev_teams/1.json
  def show
  end

  # GET /dev_teams/new
  def new
    @dev_team = DevTeam.new
  end

  # GET /dev_teams/1/edit
  def edit
  end

  # POST /dev_teams
  # POST /dev_teams.json
  def create
    @dev_team = DevTeam.new(dev_team_params)
    @dev_team_user = @dev_team.dev_teams_users.new(user_id: current_user.id)

    respond_to do |format|
      if @dev_team.save and @dev_team_user
        format.html { redirect_to @dev_team, notice: 'Dev team was successfully created.' }
        format.json { render :show, status: :created, location: @dev_team }
      else
        format.html { render :new }
        format.json { render json: @dev_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dev_teams/1
  # PATCH/PUT /dev_teams/1.json
  def update
    respond_to do |format|
      if @dev_team.update(dev_team_params)
        format.html { redirect_to @dev_team, notice: 'Dev team was successfully updated.' }
        format.json { render :show, status: :ok, location: @dev_team }
      else
        format.html { render :edit }
        format.json { render json: @dev_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dev_teams/1
  # DELETE /dev_teams/1.json
  def destroy
    @dev_team.destroy
    respond_to do |format|
      format.html { redirect_to dev_teams_url, notice: 'Dev team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def invite
    @dev_team = DevTeam.find(params[:id])
  end

  def invite_to_dev_team
    user = User.find_by_email(params[:email])
    if user
      dev_team_user = DevTeamsUser.new(dev_team_id: @dev_team.id, user_id: user.id)
      dev_team_user.save
      redirect_to dev_team_path(@dev_team), notice: 'User added in this Dev Team'
    else
      redirect_to controller: 'users', action: 'develop_invite', notice: 'User not found in Database. Please invite by email'
    end
  end

  def exclude_from_dev_team
    dev_team_user = DevTeamsUser.where(dev_team_id: params[:id], user_id: params[:user_id])
    dev_team_user.destroy_all
    redirect_to dev_team_path(id: params[:id]), notice: 'User excluded from this Dev Team'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dev_team
      @dev_team = DevTeam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dev_team_params
      params.require(:dev_team).permit(:name, :description)
    end
end
