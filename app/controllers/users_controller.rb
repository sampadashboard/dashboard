class UsersController < ApplicationController
  before_action :verify_user_token, only: [:index]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user, only: [:show, :edit, :update, :destroy, :index]
  #before_action :has_dev_permissions, only: [:show, :edit, :update, :destroy, :index]


  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.usertype = 0
    respond_to do |format|
      if @user.save
        dashboard = UserDashboard.new user_id: @user.id, description: 'Default'
        dashboard.save!
        log_in (@user)
        format.html {redirect_to root_path, notice: 'User was successfully created.'}
        format.json {render :show, status: :created, location: @user}
      else
        format.html {render :new}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  #get
  def develop_invite
    if (current_user == nil)
      respond_to do |format|
        format.html {redirect_to "/", notice: 'You must log in' and return}
      end
    end
    if (current_user.usertype == 0)
      respond_to do |format|
        format.html {redirect_to root_path, notice: 'You do not have the rights to invite a developer' and return}
      end
    end
    @user = User.new
  end

  #post
  def develop_invite2
    @temp = params[:user]
    user = User.all.find_by_email(@temp[:email])
    if (user != nil)
      if (user.usertype == 1)
        respond_to do |format|
          format.html {redirect_to root_path, notice: 'This email is already registered as a developer' and return}
        end
      else
        user.usertype = 1
        user.save!
      end
    else
      exist = Token.all.find_by_email(@temp[:email])
      if (exist)
        exist.delete
      end
      token = SecureRandom.uuid
      inv_by = (User.all.find_by_id(session[:user_id])).email
      invite = Token.new token: token, email: @temp[:email], invited_by: inv_by
      invite.save!
      @base = request.base_url
      if @base.include? "localhost"
        @iplocal = Socket.ip_address_list[1].ip_address
        @base = "http://"+@iplocal+":3000"
      end
      @reg_link = @base+"/invite/"+ token
      DevNotifierMailer.dev_invite(@temp[:email], @reg_link).deliver
      flash[:alert] = "Developer Invited"
      redirect_to root_path
    end

  end

  def invite_signup
    @id = Token.all.find_by_token(params[:rand_token])
    if (@id == nil)
      flash[:notice] = "Link invalido!"
      redirect_to '/' and return
    end
    @user = User.new()
  end


  def invite_signup2
    @user = User.new(user_params.permit(:name, :email, :token, :password))
    @user.usertype = 1
    @temp = params[:user]
    @id = Token.all.find_by_token(@temp[:token])
    @id.delete
    @user.save!
    log_in (@user)
    respond_to do |format|
      format.html {redirect_to root_path, notice: 'User was successfully created.' and return}
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html {redirect_to @user, notice: 'User was successfully updated.'}
        format.json {render :show, status: :ok, location: @user}
      else
        format.html {render :edit}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html {redirect_to users_url, notice: 'User was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)

  end
end
