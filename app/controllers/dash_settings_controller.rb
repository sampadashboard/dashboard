class DashSettingsController < DashboardController

  before_action :authenticate_user
  before_action :define_dashboard
  before_action :set_session
  before_action :verify_dashboard_owner, only: [:delete_dashboard, ]

  def settings
    @dashboards = current_user.user_dashboards.order(:id)
    verify_if_updated @dashboards
    @current_dash = current_dashboard
  end

  def create_user_dashboard
    begin
      dashboard = UserDashboard.new user_id: current_user.id, description: params[:name]
      dashboard.save!
      render json: dashboard
    rescue => e
    
      puts "ERRO: #{e}"
    
      #render plain: e, status: :unauthorized
    end
  end

  def delete_dashboard
    begin
      dashboard_id = params[:dashid]
      windows = Window.where(user_dashboard_id: dashboard_id)
      windows.each do |w|
        w.delete
      end
      UserDashboard.find(dashboard_id).delete
      render json: true
    rescue => e
      render plain: e, status: :unauthorized
    end
  end
  
  def set_current_dash
    begin
      session[:user_dashboard_id] = params[:dashid]
      current_dashboard
      redirect_to dash_settings_path
    end
  end

  private

  def define_dashboard
    if not current_dashboard
      if current_user.user_dashboards.empty?
        dashboard = current_user.user_dashboards.create(description: 'Default')
        session[:user_dashboard_id] = dashboard.id
      elsif not current_dashboard
        dashboard = current_user.user_dashboards.first
        session[:user_dashboard_id] = dashboard.id
      end
    end
  end
end
