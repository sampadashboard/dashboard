class AppsController < ApplicationController

  before_action :authenticate_user, only: [:index, :show, :new, :edit, :destroy]
  before_action :has_dev_permissions, only: [:show, :new, :edit, :update, :destroy]
  before_action :set_app, only: [:show, :edit, :update, :destroy]
  before_action :set_dashboard
  before_action :set_dev_team

  # GET /apps
  # GET /apps.json
  def index
    @apps = App.all
  end

  # GET /apps/1
  # GET /apps/1.json
  def show
  end

  # GET /apps/new
  def new
    @app = App.new
  end

  # GET /apps/1/edit
  def edit
  end

  # POST /apps
  # POST /apps.json
  def create
    @app = current_dev_team.apps.new(app_params)
    respond_to do |format|
      if @app.save
        format.html {redirect_to @app, notice: 'App was successfully created.'}
        format.json {render :show, status: :created, location: @app}
      else
        format.html {render :new}
        format.json {render json: @app.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /apps/1
  # PATCH/PUT /apps/1.json
  def update
    respond_to do |format|
      if @app.update(app_params)
        format.html {redirect_to @app, notice: 'App was successfully updated.'}
        format.json {render :show, status: :ok, location: @app}
      else
        format.html {render :edit}
        format.json {render json: @app.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.json
  def destroy
    @app.destroy
    respond_to do |format|
      format.html {redirect_back fallback_location: apps_url, notice: 'App was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_app
    @app = App.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def app_params
    params.require(:app).permit(:name, :version, :description, :base_url, :grid_size, :parameters)
  end

end
