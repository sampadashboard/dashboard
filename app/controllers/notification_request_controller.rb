require 'json'
require 'securerandom'

class NotificationRequestController < ApplicationController

  before_action :authenticate_user
  before_action :set_app, only: [:new_request_application]

  def see_requests
    @tokens = NotificationToken.where(user_id: session[:user_id]).order('title')
  end

  def delete_request
    begin
      token = NotificationToken.find params[:token_id]
      raise "Attempt to delete another user notification request!" if token.user_id != session[:user_id]
      NotificationApp.where(notification_token_id: params[:token_id]).destroy_all
      token.destroy
      redirect_to action: 'see_requests'
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  def delete_request_app
    begin
      not_app = NotificationApp.find params[:not_app_id]
      token = NotificationToken.find not_app.notification_token_id
      raise "Attempt to delete another user notification request!" if token.user_id != session[:user_id]
      not_app.destroy
      token.destroy if token.notification_apps.size == 0
      redirect_to action: 'edit_request', token_id: token.id
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  def new_request
    render layout: "notification-request"
  end

  def post_request
    alarm, token = mount_infocity_alarm(params[:request], params[:capabilities],
                                        params[:comparators], params[:capability_values])

    uri = URI(Rails.configuration.infocity_url)
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/json'})
    req.body = alarm
    https.request req

    if params[:request][:dashboard] == "S"
      redirect_to action: 'edit_request', token_id: token.id
    else
      redirect_to "/"
    end
  end

  def edit_request
    begin
      @token = NotificationToken.find params[:token_id]
      raise "Attempt to delete another user notification request!" if @token.user_id != session[:user_id]
      @apps = App.order('name')
      @notification_apps = NotificationApp.where(notification_token_id: params[:token_id])
    rescue => e
      render plain: e, status: :unauthorized
    end
  end

  def new_request_application
  end

  def post_request_application
    app_params = eval(current_app.parameters).keys
    query_params = params[:params] ? params[:params].permit(app_params).to_h : {}

    url =
      if current_app.base_url['?']
        current_app.base_url + '&' + CGI.unescape(query_params.to_query)
      else
        current_app.base_url + '?' + CGI.unescape(query_params.to_query)
      end

    NotificationApp.create notification_token_id: params[:token_id], app_id: params[:app_id],
                           query_url: url
    redirect_to action: 'edit_request', token_id: params[:token_id]
  end

  private

  def mount_infocity_alarm(request, capabilities, comparators, values)
    email = request[:email] == "S"
    push = request[:dashboard] == "S"
    token = new_token
    hook = push ? push_notification_send_requested_url + "?token=" + token.token : ""

    infocity_params = {
      alarm: {
        name: request[:title],
        user_id: current_user_token,
        latitude: request[:latitude].to_f,
        longitude: request[:longitude].to_f,
        email: email,
        push: push,
        webhook: hook,
        events_attributes: []
      }
    }

    if capabilities
      for i in capabilities
        if capabilities[i] != ""
          infocity_params[:alarm][:events_attributes] << {
            capability: capabilities[i],
            comparator: comparators[i],
            value: values[i]
          }
        end
      end
    end

    return infocity_params.to_json, token
  end

  def new_token
    begin
      token = SecureRandom.hex + SecureRandom.hex
      notification_token = NotificationToken.new user_id: session[:user_id], token: token,
                                                 state: NotificationToken::RequestedState, title: params[:request][:title]
      notification_token.save!
      return notification_token
    rescue => e
      # Violation of uniqueness: just try another key :)
      return new_token
    end
  end

end
