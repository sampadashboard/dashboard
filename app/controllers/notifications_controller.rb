require 'net/http'

class NotificationsController < ApplicationController

  before_action :authenticate_user, only: [:push_notification_form, :push_notification_send, :user_lost_notifications]
  before_action :has_dev_permissions, only: [:push_notification_form, :push_notification_send]

  def register_user
    token = params[:token]

    uri = URI("https://iid.googleapis.com/iid/v1/#{token}/rel/topics/sampadashboard")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/json'})
    req['Authorization'] = Rails.configuration.firebase_server_key
    https.request(req)

    if session[:user_id]
      user = User.find session[:user_id]
      user.firebase_token = token
      user.save!
    end

    head :ok
  end

  def push_notification_form
  end

  def push_notification_send
    payload = params.permit(:title, :message, :url, :dev_token)
    PushNotification.create payload
    payload[:action] = root_url + "last-from-sampadash"
    payload[:type] = 'general'
    send_notification payload, to = "/topics/sampadashboard"
    redirect_to "/push-notification"
  end

  # came from infocity webhook
  def push_notification_send_requested
    request = NotificationToken.find_by_token params[:token]
    request.state = NotificationToken::SendState
    request.save!

    requester = User.find request.user_id
    data = {
      title: "Sampa Dashboard Notification",
      message: request.title,
      action: root_url + "last-notifications",
      type: 'requested'
    }
    send_notification data, to = requester.firebase_token

    head :ok
  end

  # fallback for pulling when no Push Notifications available
  def new_notifications
    render json: NotificationToken.where(user_id: session[:user_id], state: NotificationToken::SendState).size
  end

  # notifications dashboard pages
  def last_notifications
  end

  def last_from_sampadash
  end

  # ajax-loading
  def load_last_notifications
    notifications =
      NotificationToken.where(
        user_id: session[:user_id], state: NotificationToken::SendState)
        .or(NotificationToken.where(
          user_id: session[:user_id], state: NotificationToken::SeenState))

    for n in notifications
      if n.state == NotificationToken::SendState
        n.state = NotificationToken::SeenState
        n.save!
      end
    end

    render json: notifications.as_json(include: :notification_apps)
  end

  def load_last_from_sampadash
    render json: PushNotification.where("created_at >= ?", Date.today - 1).order('id DESC')
  end

  private

  def send_notification(data, to)
    uri = URI("https://fcm.googleapis.com/fcm/send")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' => 'application/json'})
    req['Authorization'] = Rails.configuration.firebase_server_key
    req.body = {
      data: {
        title: data[:title],
        message: data[:message],
        url: data[:url],
        type: data[:type]
      },
      notification: {
        title: data[:title],
        body: data[:message],
        click_action: data[:action],
        icon: "/sampa-dashboard-logo.png"
      },
      to: to
    }.to_json

    https.request(req)
  end

end
