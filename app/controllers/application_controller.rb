class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionsHelper

  private

  def current_user
    @current_user ||= if session[:user_id]
                        User.find_by_id(session[:user_id])
                      end
  end

  def has_dev_permissions
    if current_user.usertype == 0
      redirect_to root_path and return
    end
  end

  def authenticate_user
    if not current_user
      redirect_to visitors_index_path and return
    end
  end

  def set_dashboard
    session[:user_dashboard_id] = params[:user_dashboard_id] if params[:user_dashboard_id]
  end

  def current_dashboard
    @current_dashboard ||= if session[:user_dashboard_id]
                             UserDashboard.find_by_id(session[:user_dashboard_id])
                           end
  end

  def verify_dashboard_owner
    if not current_dashboard or (current_dashboard.user_id != current_user.id)
      redirect_to root_path, notice: 'Attempt to access another user dashboard detected!' and return
    end
  end

  def set_dev_team
    session[:dev_team_id] = params[:dev_team_id] if params[:dev_team_id]
  end

  def current_dev_team
    @current_dev_team ||= if session[:dev_team_id]
                            DevTeam.find_by_id(session[:dev_team_id])
                          end
  end

  def set_app
    session[:app_id] = params[:app_id] if params[:app_id]
  end

  def current_app
    @current_app ||= if session[:app_id]
                       App.find_by_id(session[:app_id])
                     end
  end

  def current_window
    @current_window ||= if params[:id]
                          Window.find_by_id(params[:id])
                        end
  end

  def verify_window_owner
    if not current_window or (current_window.user_dashboard_id != current_dashboard.id)
      redirect_to root_path, notice: 'invalid action' and return
    end
  end

  def current_user_token
    BCrypt::Password.create(current_user.email)
  end

  def verify_user_token
    user_token = params[:user_token]
    if user_token and user_token.size == 60

      User.all.each do |user|
        if BCrypt::Password.new(user_token) == user.email
          render json: {email: user.email, phone: user.fone}
        end
      end
    end
  end

  helper_method :current_user, :authenticate_user, :set_dashboard, :current_dashboard, :verify_dashboard_owner, :set_dev_team, :current_dev_team, :set_app, :current_app, :current_window, :verify_window_owner, :current_user_token, :verify_user_token

end
