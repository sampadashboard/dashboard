class WindowsController < ApplicationController
  before_action :set_window, only: [:show, :edit, :update, :destroy]
  before_action :set_app, except: [:show, :destroy]
  before_action :set_dashboard
  before_action :authenticate_user
  before_action :verify_dashboard_owner
  before_action :verify_window_owner, except: [:index, :new, :create, :update]

  require 'open-uri'

  # GET /windows
  # GET /windows.json
  def index
    @windows = Window.all
  end

  # GET /windows/1
  # GET /windows/1.json
  def show
    redirect_to root_path and return
  end

  # GET /windows/new
  def new
    @window = current_app.windows.new(col: 1, row: 1, size_x: get_size_x, size_y: get_size_y, app_id: current_app.id, app_version: current_app.version, user_dashboard_id: current_dashboard.id)
  end

  # GET /windows/1/edit
  def edit
  end

  # POST /windows
  # POST /windows.json
  def create
    @window = current_app.windows.new(window_params)

    respond_to do |format|
      if @window.save
        format.html {redirect_to root_path, notice: 'Window was successfully created.'}
        format.json {render :show, status: :created, location: @window}
      else
        format.html {render :new}
        format.json {render json: current_app.errors, status: :unprocessable_entity}
      end
    end
  end
  
  # PATCH/PUT /windows/1
  # PATCH/PUT /windows/1.json
  def update
     @window.update(window_params)
     head :ok
  end

  # DELETE /windows/1
  # DELETE /windows/1.json
  def destroy
    @window.destroy
    head :ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_window
    @window = Window.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def window_params
    params[:window][:query_url] ||= get_query_url
    params.require(:window).permit(:col, :row, :size_x, :size_y, :query_url, :user_dashboard_id, :app_id, :app_version)
  end

  def get_query_url
    app_params = eval(current_app.parameters).keys
    query_params = params.require(:window).permit(app_params).to_h

    if current_app.base_url['?']
      current_app.base_url + '&' + CGI.unescape(query_params.to_query)
    else
      current_app.base_url + '?' + CGI.unescape(query_params.to_query)
    end
  end

  def get_size_x
    current_app.grid_size.split('x').map(&:to_i)[0]
  end

  def get_size_y
    current_app.grid_size.split('x').map(&:to_i)[1]
  end
end
