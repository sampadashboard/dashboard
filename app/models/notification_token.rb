class NotificationToken < ApplicationRecord

  belongs_to :user
  validates_uniqueness_of :token
  has_many :notification_apps

  RequestedState = '1' # user requests a notification
  SendState = '2' # notification is sent by Infocity
  SeenState = '3' # notification was seen by user

end
