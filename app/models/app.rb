class App < ApplicationRecord
  has_many :windows, dependent: :destroy
  belongs_to :dev_team
  validates :name, presence: true
  validates :version, presence: true
  validates :description, presence: true
  validates :base_url, presence: true
  validates :grid_size, presence: true
  validates :parameters, presence: true
end
