class DevTeamsUser < ApplicationRecord
  belongs_to :user
  belongs_to :dev_team
end
