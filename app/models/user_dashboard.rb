class UserDashboard < ApplicationRecord
  belongs_to :user
  has_many :windows, dependent: :destroy

  validates_presence_of :description, message: 'Missing dashboard description'
  validates_length_of :description, :maximum => 15, :allow_blank => false
end
