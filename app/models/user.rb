class User < ApplicationRecord

  has_many :user_dashboards, dependent: :destroy
  has_many :dev_teams_users
  has_many :dev_teams, through: :dev_teams_users


  def self.from_omniauth(auth)
    user = User.find_by_email (auth.info.email)
    if (user)
      return user;
    end
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.email = auth.info.email
      user.usertype = 0
      user.save!
      dashboard = UserDashboard.new user_id: user.id, description: 'Default'
      dashboard.save!
    end
  end
end
