class DevTeam < ApplicationRecord
  has_many :dev_teams_users
  has_many :users, through: :dev_teams_users
  has_many :apps, dependent: :destroy
end
