json.extract! app, :id, :name, :version, :developer, :description, :base_url, :grid_size, :parameters, :created_at, :updated_at
json.url app_url(app, format: :json)
