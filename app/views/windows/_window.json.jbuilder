json.extract! window, :id, :col, :row, :size_x, :size_y, :query_url, :user_dashboard_id, :app_id, :created_at, :updated_at
json.url window_url(window, format: :json)
