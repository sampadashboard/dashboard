json.extract! dev_team, :id, :name, :description, :created_at, :updated_at
json.url dev_team_url(dev_team, format: :json)
