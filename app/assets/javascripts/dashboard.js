var gridster;
var csrfToken;
var gridsterConf = {
  widget_margins: [5, 5],
  widget_base_dimensions: [140, 140],
  shift_widgets_up: false,
  shift_larger_widgets_down: false,
  collision: {wait_for_mouseup: true},
  helper: 'clone',
  draggable: {
    handle: 'header'
  },
  resize: {
    enabled: true
  }
};

function configureGridsterDefaultDashboard() {
  gridsterConf.resize.start = windowStartingResize;
  gridsterConf.resize.resize = windowResizing;
  gridsterConf.resize.stop = windowResized;
  configureGridster();
}

function configureGridsterUserDashboard() {
  gridsterConf.draggable.stop = draggableStop;
  gridsterConf.draggable.start = draggableStart;
  gridsterConf.resize.start = windowStartingResize;
  gridsterConf.resize.resize = windowResizing;
  gridsterConf.resize.stop = windowResized;
  configureGridster();
}

function configureGridster() {
  gridster = $(".gridster ul").gridster(gridsterConf).data('gridster');
}

function loadDashboardWindows() {
  $.get(
    "/current-dashboard",
    function (windows) {
      gridster.remove_all_widgets();

      for (var w in windows)
        addWindow(windows[w]);
    }
  );
}

function addWindow(window) {
  var html =
    '<li data-id="' + window.id + '" data-query_url="' + window.query_url + '" >' +  // keeping window ID
    '<header>' + window.app.name + '<div class="window_delete">x</div></header>' +
    '<iframe src="' + window.query_url + '" scrolling="no"></iframe>' +
    '</li>';
  var newWindow = gridster.add_widget(html, window.size_x, window.size_y, window.col, window.row);
  adjustIframeDimensions(newWindow);
}

function addNotificationWindow(data) {
  var html =
    (data.not_app ? '<li data-app="' + data.not_app + '">' : '<li>') +
    '<header>' + data.title + ' <div class="window_delete html_window_delete">x</div></header>' +
    '<iframe src="' + data.url + '" scrolling="no"></iframe>' +
    '</li>';
  var newWindow = gridster.add_widget(html, 2, 2, 0, 0);
  adjustIframeDimensions(newWindow);
}

function deleteWindow(event) {
  var widget = event.target.parentElement.parentElement;

  if (widget.dataset.id) {
    $.ajax({
      type: 'delete',
      url: "/windows/" + widget.dataset.id,
      headers: {
        "X-CSRF-Token": csrfToken
      },
      complete: function () {
        widget.remove();
      }
    });
  }
  else if (widget.dataset.app) {
    $.ajax({
      type: 'get',
      url: "/delete-notification-app/" + widget.dataset.app,
      complete: function () {
        widget.remove();
      }
    });
  }
  else
    widget.remove();
}

function deleteHTMLWindow(event) {
  var widget = event.target.parentElement.parentElement;
  widget.remove();
}

function draggableStop(event, ui) {
  var data = ui.$helper[0].dataset;  // id + gridster window values :)
  if (data.id) postWindowUpdate(data);
  $('.overlay_fix').hide();
}

function draggableStart(event, ui) {
  $('.overlay_fix').show();
}

function windowStartingResize(event, ui, widget) {
  var iframe = $(widget).children("iframe");
  iframe.css('pointer-events', 'none');
}

function windowResizing(event, ui, widget) {
  adjustIframeDimensions(widget);
}

function windowResized(event, ui, widget) {
  var data = widget[0].dataset;
  if (data.id) postWindowUpdate(data);
  adjustIframeDimensions(widget);
  var iframe = $(widget).children("iframe");
  iframe.css('pointer-events', 'auto');
}

function postWindowUpdate(data) {
  $.ajax({
    type: 'patch',
    url: "/windows/" + data.id,
    headers: {
      "X-CSRF-Token": csrfToken
    },
    data: {
      window: {
        col: data.col,
        row: data.row,
        size_x: data.sizex,
        size_y: data.sizey,
        query_url: data.query_url
      }
    }
  });
}

function adjustIframeDimensions(widget) {
  var iframe = $(widget).children("iframe");
  iframe.css({
    width: (widget[0].offsetWidth - 6) + 'px',
    height: (widget[0].offsetHeight - 26) + 'px'
  });
}
