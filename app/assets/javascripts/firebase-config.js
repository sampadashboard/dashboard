firebase.initializeApp({
  apiKey: "AIzaSyBI4qAr40eI0ZDbcLiGMoAfMQ_826yNKw4",
  messagingSenderId: "341587114548"
});
var messaging = firebase.messaging();


messaging.requestPermission()
  .then(function () {
    console.log('Firebase permission granted.');
    permissionGranted();
  })
  .catch(function (err) {
    console.log("Firebase permission denied -- some pulling will be needed");
    fallback();
  });


function permissionGranted() {
  messaging.getToken()
    .then(function (currentToken) {
      if (currentToken) {
        sendTokenToServer(currentToken);
      }
      else
        console.log('No Instance ID token available. Request permission to generate one.');
    })
    .catch(function (err) {
      console.log('An error occurred while retrieving token.', err);
    });
}


messaging.onTokenRefresh(function () {
  messaging.getToken()
    .then(function (refreshedToken) {
      sendTokenToServer(refreshedToken);
    })
    .catch(function (err) {
    });
});


function sendTokenToServer(token) {
  $.ajax({
    type: 'post',
    url: "/register-user",
    data: {
      token: token
    },
    headers: {
      "X-CSRF-Token": csrfToken
    },
    complete: function () {
      registerServiceWorker();
    }
  });
}

function registerServiceWorker() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/firebase-messaging-sw.js',
      {scope: '/firebase-cloud-messaging-push-scope'})
      .then(function (reg) {

        navigator.serviceWorker.addEventListener('message', function (payload) {
          showNotification(payload.data['firebase-messaging-msg-data'].data);
        });

      })
      .catch(function (err) {
        console.error("SW registration failed with error " + err);
      });
  }

}

function showNotification(data) {
  $('#notifications_link').css('color', '#d9534f');
  $('#notifications_normal').css('display', 'none');
  $('#notifications_alert').css('display', 'block');

  if (data.type == 'requested')
    $('#lost_notifications_link li').css('color', '#d9534f');
  else
    $('#last_24h_link li').css('color', '#d9534f');
}
