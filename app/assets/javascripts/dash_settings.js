function okDashboardButton(event) {
  event.preventDefault();

  $.ajax({
    type: 'post',
    url: "/dashboard/new/" + $("#dashboard_name").val(),
    headers: {
      "X-CSRF-Token": csrfToken
    },
    complete: function (dashboard) {
      var id = dashboard.responseJSON.id;
      var description = dashboard.responseJSON.description;
      $("#dashboard_select").append('<option value="' + id + '">' + description + '</option>').val(id);
      updateNewWindowLink(id);
      $.ajax({
        type: 'post',
        url: '/set_new_dash/' + id,
        headers: {
          "X-CSRF-Token": csrfToken
        }
      });
    }
  });
}

function deleteDashboard(event) {
  var selected = $("#delete_select").find(":selected");

  if (selected.length == 0 || !confirm("Delete the dashboard " + selected.text() + "?"))
    return;

  var dashId = selected.val();
  $.ajax({
    type: 'post',
    url: '/dashboard/' + dashId + '/delete',
    headers: {
      "X-CSRF-Token": csrfToken
    },
    complete: function (resp) {
      if (resp) {
        selected.remove();
        selected = $("#dashboard_select").find(":selected");
        if (selected) {
          updateNewWindowLink(selected.val());
        }
        location.reload(true);
      }
      else alert(resp);
    }
  });
}


function setCurrentDash() {
  var dashId = $("#dashboard_select").val();


  $.ajax({
    type: 'post',
    url: '/set_new_dash/' + dashId,
    headers: {
      "X-CSRF-Token": csrfToken
    }
  });
}

function updateNewWindowLink(dashboardId) {
  $("#new_window_link").attr("href", "/apps?user_dashboard_id=" + dashboardId);
}
