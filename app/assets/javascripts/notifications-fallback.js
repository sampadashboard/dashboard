var INTERVAL = 30000;

function fallback() {
  setTimeout(checkServerNotifications, INTERVAL);
}

function checkServerNotifications() {
  $.ajax({
    type: 'get',
    url: "/new-notifications",
    complete: function (notifications) {
      if (notifications.responseJSON > 0)
        showNotifications({type: 'requested'});
    }
  });
  setTimeout(checkServerNotifications, INTERVAL);
}
