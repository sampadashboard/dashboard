Rails.application.routes.draw do

  get 'visitors/index'
  root 'dashboard#dashboard'
  resources :windows
  resources :apps
  resources :users
  resources :sessions, only: [:create, :destroy]
  resources :dev_teams
  get 'dashboard' => 'dashboard#dashboard'
  get 'dashboard/listapps' => 'dashboard#list_apps'
  get 'current-dashboard' => 'dashboard#load_user_dashboard'
  post 'dashboard/new/:name' => 'dash_settings#create_user_dashboard'
  post 'dashboard/:dashid/delete' => 'dash_settings#delete_dashboard'
  
  get '/window-proxy' => 'dashboard#window_proxy'

  get 'invite/:rand_token', to: 'users#invite_signup'
  post 'users/invite/new', to: 'users#invite_signup2'
  get '/invite', to: 'users#develop_invite'
  post 'invite', to: 'users#develop_invite2'
  post 'invite_dev_to_team', to: 'dev_teams#invite_to_dev_team'
  get 'exclude_dev_from_team', to: 'dev_teams#exclude_from_dev_team'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create2'
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  get '/new-notification-request' => 'notification_request#new_request'
  get '/notification-requests' => 'notification_request#see_requests'
  get '/notification-requests/:token_id' => 'notification_request#edit_request'
  post '/notification-requests' => 'notification_request#post_request'
  get '/delete-notification-request/:token_id' => 'notification_request#delete_request'
  get '/delete-notification-app/:not_app_id' => 'notification_request#delete_request_app'
  get '/new-request-app' => 'notification_request#new_request_application'
  post '/request-app' => 'notification_request#post_request_application'

  post '/register-user' => 'notifications#register_user'
  get '/push-notification' => 'notifications#push_notification_form'

  post '/push-notification-send' => 'notifications#push_notification_send'
  get '/push-notification-send-requested' => 'notifications#push_notification_send_requested'

  get '/new-notifications' => 'notifications#new_notifications'
  get '/last-notifications' => 'notifications#last_notifications'
  get '/last-from-sampadash' => 'notifications#last_from_sampadash'
  get '/load-last-notifications' => 'notifications#load_last_notifications'
  get '/load-last-from-sampadash' => 'notifications#load_last_from_sampadash'

  get '/dash_settings' => 'dash_settings#settings'
  post '/set_new_dash/:dashid' => 'dash_settings#set_current_dash'

end
