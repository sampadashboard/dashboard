require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Dashboard
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.action_dispatch.default_headers = {
      'X-Frame-Options' => 'ALLOWALL'
    }
    
    config.firebase_server_key =  'key=AAAAT4gtujQ:APA91bExvT9FL_Y3gFqVszVV4NNU_t6MOTAVZW2Kz7WMXRDoCF_5XjVRCe9N0TkHCWch42OUiB3042e3dXC-weK9zcraBdhBRTgCuFeP84MIgj036HQ_TBQM1KmvHliG50IgNPIGd2JB'
    
    config.infocity_url = "https://infocity-poo.herokuapp.com/alarms.json"

  end
end
