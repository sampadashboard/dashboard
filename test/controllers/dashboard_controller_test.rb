require 'test_helper'
require 'uri'

class DashboardControllerTest < ActionDispatch::IntegrationTest

  test "should show a default dashboard for a non-logged user" do
    logout
    assert_redirected_to visitors_index_path
  end

  test "should show user dashboard" do
    user = create_user
    dashboard = create_dashboard(user)
    login user
    follow_redirect!
    assert_response :success
    assert_select 'a', 'Sign out'
    assert_select '.nav-side-menu'
  end

  test "user dashboard windows as json for ajax calls" do
    user = create_user
    dashboard = create_dashboard(user)

    login user

    get "/current-dashboard"
    assert_response :success
    json = JSON.parse(response.body)

    assert_equal 2, json.size
    assert_equal 'Graph', json[0]['app']['name']
    assert_equal 'Temperature', json[1]['app']['name']
    assert_equal dashboard.id, json[0]['user_dashboard_id']
    assert_equal dashboard.id, json[1]['user_dashboard_id']
  end

  test "don't allow a non-logged user to access some dashboard" do
    logout
    get "/current-dashboard"
    assert_redirected_to visitors_index_path
  end

  test 'outdated app must be updated' do
    user = create_user
    dash = create_dashboard(user)
    app = App.last
    window = Window.last
    update_an_app(app.name)
    login(user)
    follow_redirect!
    assert_redirected_to edit_window_path(id: window.id, app_id: app.id)
  end

  private

  def assert_original_dashboard_state(dashboard)
    windows = Window.where(user_dashboard_id: dashboard.id).order(:id)
    assert_equal 2, windows.size
    assert_equal 'Graph', windows[0].app.name
    assert_equal 'Temperature', windows[1].app.name
  end

end
