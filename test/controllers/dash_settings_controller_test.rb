require 'test_helper'

class DashSettingsControllerTest < ActionDispatch::IntegrationTest

  test "creating a new dashboard" do
    dashname = "My New Big Dash"
    dashencode = URI.encode(dashname)
    user = create_user

    login user
    post "/dashboard/new/#{dashencode}"
    assert_response :success

    dashboard = UserDashboard.where(description: dashname).first
    assert_equal user.id, dashboard.user_id
  end

  test "don't allow a non-logged user to create a new dashboard" do
    dashname = "My New Big Dashboard"
    dashencode = URI.encode(dashname)

    logout
    post "/dashboard/new/#{dashencode}"
    assert_redirected_to visitors_index_path

    assert_equal 0, UserDashboard.where(description: dashname).size
  end

  test "deleting a dashboard must remove all its windows" do
    user = create_user
    dash1 = create_dashboard(user)
    dash2 = create_dashboard(user)
    all_windows = Window.where(user_dashboard_id: [dash1.id, dash2.id])
    assert_equal 4, all_windows.size

    login user
    post "/dashboard/#{dash1.id}/delete"
    assert_response :success

    all_windows = Window.where(user_dashboard_id: [dash1.id, dash2.id])
    assert_equal 2, all_windows.size
    assert_equal dash2.id, all_windows[0].user_dashboard_id
    assert_equal dash2.id, all_windows[1].user_dashboard_id
  end

  test "don't allow a non-logged user to delete a dashboard" do
    user = create_user
    dash = create_dashboard(user)

    logout
    post "/dashboard/#{dash.id}/delete"
    assert_redirected_to visitors_index_path

    dashs = UserDashboard.where(user_id: user.id)
    assert_equal 1, dashs.size

    windows = Window.where(user_dashboard_id: dashs[0].id)
    assert_equal 2, windows.size
  end

  test "don't allow an user to delete another user's dashboard" do
    user = create_user
    dash = create_dashboard(user)

    login create_another_user
    post "/dashboard/#{dash.id}/delete"
    assert_redirected_to root_path

    dashs = UserDashboard.where(user_id: user.id)
    assert_equal 1, dashs.size

    windows = Window.where(user_dashboard_id: dashs[0].id)
    assert_equal 2, windows.size
  end

end
