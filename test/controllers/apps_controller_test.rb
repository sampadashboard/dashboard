require 'test_helper'

class AppsControllerTest < ActionDispatch::IntegrationTest
  setup do
    logout
    @user_one = create_developer_user
    @dashboard_one = create_dashboard(@user_one)
    @dev_team = DevTeam.last
    @app_one = App.last
    login(@user_one)
    follow_redirect!
  end

  test "should get index" do
    get apps_url
    assert_response :success
  end

  test "should get new" do
    get new_app_url
    assert_response :success
  end

  test "should create app" do
    assert_difference('App.count') do
      post apps_url, params: {app: {name: 'test_app', version: '1.0', description: 'test_description', base_url: 'test_base_url', grid_size: '1x2', parameters: "{field_name: 'text_field'}"}, dev_team_id: @dev_team.id}
    end

    assert_redirected_to app_url(App.last)
  end

  test "should show app" do
    get app_url(@app_one)
    assert_response :success
  end

  test "should get edit" do
    get edit_app_url(@app_one)
    assert_response :success
  end

  test "should update app" do
    patch app_url(@app_one), params: {app: {name: 'test_app', version: '1.0', description: 'test_description', base_url: 'test_base_url', grid_size: '2x3', parameters: "{field_name: 'text_field'}"}}
    assert_redirected_to app_url(@app_one)
  end

  test "should destroy app" do
    assert_difference('App.count', -1) do
      delete app_url(@app_one)
    end

    assert_redirected_to apps_url
  end
end
