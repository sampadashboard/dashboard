require 'test_helper'

class NotificationRequestControllerTest < ActionDispatch::IntegrationTest
  
  test_request = {
    request: {
      latitude: "0",
      longitude: "0",
      title: "Test",
      email: "N",
      dashboard: "N"
    }
  }
  
  dashboard_test_request = {
    request: {
      latitude: "0",
      longitude: "0",
      title: "Test",
      email: "N",
      dashboard: "S"
    }
  }
  
  test "a non-logged user should not be able to see the notification request form" do
    logout
    get new_notification_request_url
    assert_redirected_to visitors_index_path
  end
  
  test "a logged-in user should be able to see the notification request form" do
    login create_user
    get new_notification_request_url
    assert_response :success
  end
  
  test "a non-logged user should not be able to request a notification" do
    logout
    post "/notification-requests", params: test_request
    assert_redirected_to visitors_index_path
  end
  
  test "a logged-in user should be able to request a notification" do
    login create_user
    post "/notification-requests", params: test_request
    assert_redirected_to root_url
  end
  
  test "when requested a dashboard notification, the user should see the app select page" do
    login create_user
    post "/notification-requests", params: dashboard_test_request
    assert_response :redirect
    assert_match "/notification-requests/", @response.redirect_url 
    follow_redirect!
    assert_select "h1", "Notification Request Popup Windows"
  end
  
end
