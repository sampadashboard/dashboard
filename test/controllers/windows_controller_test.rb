require 'test_helper'

class WindowsControllerTest < ActionDispatch::IntegrationTest
  setup do
    logout
    @user_one = create_user
    login(@user_one)
    @dashboard_one = create_dashboard(@user_one)
    @app_one = App.last
    @window_one = Window.last
  end

  test "should get index" do
    get windows_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id)
    assert_response :success
  end

  test "should get new" do
    get new_window_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id)
    assert_response :success
  end

  test "should create window" do
    assert_difference('Window.count') do
      post windows_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id), params: {window: {user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, col: 1, row: 2, size_x: 3, size_y: 4, query_url: ''}}
    end

    assert_redirected_to root_url
  end

  test "should show window" do
    get window_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, id: @window_one.id)
    assert_redirected_to root_url
  end

  test "should get edit" do
    get edit_window_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, id: @window_one.id)
    assert_response :success
  end

  test "should update window" do
    patch window_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, id: @window_one.id), params: {window: {app_id: @window_one.app_id, col: @window_one.col, query_url: @window_one.query_url, row: @window_one.row, size_x: @window_one.size_x, size_y: @window_one.size_y, user_dashboard_id: @window_one.user_dashboard_id}}
    assert_response :success
  end

  test "should destroy window" do
    assert_difference('Window.count', -1) do
      delete window_url(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, id: @window_one.id)
    end
    assert_response :success
  end

  test "don't allow a non-logged user to create a new window on some dashboard" do
    user = create_user
    dashboard = create_dashboard user
    app = App.last

    logout

    post windows_url, params: {user_dashboard_id: dashboard.id, app_id: app.id}
    assert_redirected_to visitors_index_path
  end

  test "don't allow a non-logged user to update a window" do
    dashboard = create_dashboard(create_user)
    window = Window.where(user_dashboard_id: dashboard.id).order(:id).first

    assert_not_equal 3, window.col
    assert_not_equal 3, window.row
    assert_not_equal 3, window.size_x
    assert_not_equal 3, window.size_y

    logout
    patch window_url(user_dashboard_id: dashboard.id, app_id: App.last.id, id: window.id), params: {window: {col: 3, row: 3, size_x: 3, size_y: 3}}
    assert_redirected_to visitors_index_path
  end

  test "don't allow a non-logged user to delete a window" do
    dashboard = create_dashboard(create_user)
    window = Window.where(user_dashboard_id: dashboard.id).order(:id).first

    logout
    delete window_url(user_dashboard_id: dashboard.id, app_id: App.last.id, id: window.id)
    assert_redirected_to visitors_index_path
  end

  test "don't allow an user to create a new window on another user's dashboard" do
    logout
    @user_two = create_another_user
    login(@user_two)

    post windows_path(user_dashboard_id: @dashboard_one.id, app_id: @app_one.id), params: {window: {user_dashboard_id: @dashboard_one.id, app_id: @app_one.id, col: 1, row: 2, size_x: 3, size_y: 4, query_url: ''}}
    assert_redirected_to root_path
  end

  test "don't allow an user to update another user's window" do
    dashboard = create_dashboard(create_user)
    window = Window.where(user_dashboard_id: dashboard.id).order(:id).first

    assert_not_equal 3, window.col
    assert_not_equal 3, window.row
    assert_not_equal 3, window.size_x
    assert_not_equal 3, window.size_y

    login create_another_user
    patch window_url(user_dashboard_id: dashboard.id, app_id: App.last.id, id: window.id), params: {col: 3, row: 3, size_x: 3, size_y: 3}
    assert_redirected_to root_path
  end

  test "don't allow an user to delete another user's window" do
    dashboard = create_dashboard(create_user)
    window = Window.where(user_dashboard_id: dashboard.id).order(:id).first

    login create_another_user
    delete window_path(user_dashboard_id: dashboard.id, app_id: App.last.id, id: window.id)
    assert_redirected_to root_path
  end
end
