require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest

  sample_notification = {title: "Title", message: "Message", url: "http://url.com"}
  
  test "should send a token to Firebase even if user is not logged in" do
    logout
    post register_user_url, params: {token: "abc"}
    assert_response :success
  end
  
  test "should send a logged user's token and save it on the database" do
    user = create_user
    login user
    post register_user_url, params: {token: "abc"}
    assert_response :success
    retrieved_again = User.find user.id
    assert_equal "abc", retrieved_again.firebase_token
  end
  
  test "should not show a push notification form to a non-logged user" do
    logout
    get push_notification_url
    assert_redirected_to visitors_index_path
  end
  
  test "should not show the general push notification form to a non-developer user" do
    login create_user
    get push_notification_url
    assert_redirected_to root_url
  end
  
  test "should show the general push notification form to a developer user" do
    login create_developer_user
    get push_notification_url
    assert_response :success
  end
  
  test "a non-logged user should not post a push notification" do
    logout
    post push_notification_send_url, params: sample_notification
    assert_redirected_to visitors_index_path
  end
  
  test "a non-developer user should not post a push notification" do
    login create_user
    post push_notification_send_url, params: sample_notification
    assert_redirected_to root_url
  end
  
  test "a developer user shout be able to post a push notification" do
    login create_developer_user
    post push_notification_send_url, params: sample_notification
    assert_redirected_to push_notification_url
  end
  
  test "Infocity should be able to send a requested notification" do
    user = create_user
    token = NotificationToken.new title: "Test", user_id: user.id, 
                                  state: NotificationToken::RequestedState, token: "abc"
    token.save!
    get push_notification_send_requested_url + "?token=abc"
    assert_response :success
    retrieved_again = NotificationToken.find token.id
    assert_equal NotificationToken::SendState, retrieved_again.state
  end
    
end
