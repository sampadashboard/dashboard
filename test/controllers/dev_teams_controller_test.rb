require 'test_helper'

class DevTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    user = create_user
    dashboard = create_dashboard user
    login user
    follow_redirect!
    @dev_team = create_dev_team user
  end

  test "should get index" do
    get dev_teams_url
    assert_response :success
  end

  test "should get new" do
    get new_dev_team_url
    assert_response :success
  end

  test "should create dev_team" do
    assert_difference('DevTeam.count') do
      post dev_teams_url, params: { dev_team: { description: @dev_team.description, name: @dev_team.name }}
    end

    assert_redirected_to dev_team_url(DevTeam.last)
  end

  test "should show dev_team" do
    get dev_team_url(@dev_team)
    assert_response :success
  end

  test "should get edit" do
    get edit_dev_team_url(@dev_team)
    assert_response :success
  end

  test "should update dev_team" do
    patch dev_team_url(@dev_team), params: { dev_team: { description: @dev_team.description, name: @dev_team.name } }
    assert_redirected_to dev_team_url(@dev_team)
  end

  test "should destroy dev_team" do
    assert_difference('DevTeam.count', -1) do
      delete dev_team_url(@dev_team)
    end

    assert_redirected_to dev_teams_url
  end
end
