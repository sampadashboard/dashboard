require "application_system_test_case"

class DashboardInteractionsTest < ApplicationSystemTestCase

  test "visiting the index" do
    visit root_url
    assert_no_selector "a", text: "Logout"
    assert_selector "a", text: "Sign in"
    assert_selector "div.gridster li", count: 3
  end

  test "I see my dashboard after log in" do
    main_scenario
    assert_no_selector "a", text: "Sign in"
    assert_selector "a", text: "Sign out"
    assert_selector "div.gridster li", count: 2
    windows = find_all("div.gridster li")
    assert windows[0].find("header").base.inner_html["Graph"]
    assert windows[1].find("header").base.inner_html["Temperature"]
  end

  test "Creating a new window" do
    main_scenario
    dev_team = create_dev_team(User.last)
    create_one_more_app(dev_team)
    find("li", text: "Application").click
    click_link "New window"
    click_link "Clock"
    click_button "Create Window"
    assert_selector "div.gridster li", count: 3
    windows = find_all("div.gridster li")
    assert windows[0].find("header").base.inner_html["Graph"]
    assert windows[1].find("header").base.inner_html["Temperature"]
    assert windows[2].find("header").base.inner_html["Clock"]
  end

  test "Creating a new dashboard" do
    main_scenario
    find("li", text: "SampaDash Options").click
    assert_selector "#dashboard_select option", count: 1
    new_dash_name = "SecondDash"
    fill_in "dashboard_name", with: new_dash_name
    click_link "ok_dashboard_icon"
    assert_selector ".menu-list ul a:nth-child(2)", text: new_dash_name + " Dash"
    assert_selector "#dashboard_select option", count: 2
    option = find("#dashboard_select option:nth-child(2)")
    new_window_link_references option.text
  end

  test "Deleting a window" do
    main_scenario
    assert_selector "div.gridster li", count: 2
    page.execute_script('$(".gs-w").trigger("hover")')
    delete_buttons = find_all('.window_delete')
    delete_buttons[0].click
    assert_selector "div.gridster li", count: 1
    windows = find_all("div.gridster li")
    assert windows[0].find("header").base.inner_html["Temperature"]
  end

  test "Switching dashboards" do
    user = create_user
    dashboard = create_dashboard user

    another_dashboard = create_dashboard(user)
    another_dashboard.description = 'Another'
    another_dashboard.save! 
    
    dev_team = create_dev_team(user)
    another_app = create_one_more_app(dev_team)
    
    another_window = Window.new user_dashboard: another_dashboard, app: another_app, app_version: another_app.version, query_url: another_app.base_url
    another_window.save!

    login
    
    find("li", text: "SampaDash Options").click
    assert_selector "#dashboard_select option", count: 2
    select = find "#dashboard_select"
    assert_equal dashboard.id.to_s, select.value
    
    other = select.find("option:nth-child(2)")
    other.select_option
    find("#select_dashboard_btn").click
    
    assert_selector ".menu-list ul a:nth-child(2)", text: another_dashboard.description
    new_window_link_references another_dashboard.description
  end

  test "Deleting a dashboard" do
    user = create_user
    dashboard = create_dashboard(user)
    another_dashboard = create_dashboard(user)

    login
    find("li", text: "Dash Options").click
    assert_selector "#dashboard_select option", count: 2

    accept_confirm do
      click_link "Delete Dashboard"
    end

    find("li", text: "Dash Options").click
    assert_selector "#dashboard_select option", count: 1
    new_window_link_references another_dashboard.description
  end

end
