require "test_helper"
require "capybara/webkit"

Capybara::Webkit.configure do |config|
  config.block_unknown_urls
end

Capybara.server_port = 30000

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :webkit
  
  def main_scenario
    user = create_user
    create_dashboard user
    login
  end

  def login
    visit login_url
    fill_in :session_email, with: "fake@user.com"
    fill_in :session_password, with: "123456"
    click_button "Log in"
  end

  def new_window_link_references(description)
    find("li", text: "Application").click
    assert_selector ".menu-content a:nth-child(2) li", text: description + " Dash"
  end
  
end
