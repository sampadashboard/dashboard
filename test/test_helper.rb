require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

HEROKU_EXAMPLES = 'https://get-examples.herokuapp.com'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def create_user
    a = User.new email: 'fake@user.com', password: '123456', name: 'Fake User', usertype: 0
    a.save!
    return a
  end

  def create_another_user
    b = User.new email: 'another@user.com', password: 'admin', name: 'Another User'
    b.save!
    return b
  end

  def create_dev_team(user)
    dev_team = user.dev_teams.new name: 'Test Team', description: 'For test purpose only'
    dev_team.save!
    return dev_team
  end
  
  def create_developer_user
    a = User.new email: 'fake@user.com', password: '123456', name: 'Fake User', usertype: 1
    a.save!
    return a
  end

  def create_app(dev_team)
    app = dev_team.apps.new name: 'Graph', version: '1.0', description: 'shows a graph', base_url: "#{HEROKU_EXAMPLES}/graph/index", grid_size: '3x2', parameters: '{}'
    app.save!
    return app
  end

  def create_another_app(dev_team)
    app = dev_team.apps.new name: 'Temperature', version: '1.0', description: 'shows a termometer', base_url: "#{HEROKU_EXAMPLES}/temperature/index", grid_size: '1x2', parameters: '{}'
    app.save!
    return app
  end

  def create_one_more_app(dev_team)
    app = dev_team.apps.new name: 'Clock', version: '1.0', description: 'shows a clock', base_url: "#{HEROKU_EXAMPLES}/clock/index", grid_size: '1x2', parameters: '{}'
    app.save!
    return app
  end

  def update_an_app(name)
    app = App.find_by_name(name)
    app.version = '2.0'
    app.save!
    return app
  end

  def login(user)
    post login_url, params: {session: {email: user.email, password: user.password}}
  end

  def logout
    get signout_url
    follow_redirect!
    assert_response :redirect
  end

  def create_dashboard(user)
    dev_team = create_dev_team user
    app1 = create_app dev_team
    app2 = create_another_app dev_team

    dashboard = UserDashboard.new description: 'Fake', user: user
    dashboard.save!

    window1 = Window.new user_dashboard: dashboard, app: app1, app_version: app1.version, query_url: app1.base_url
    window1.save!
    window2 = Window.new user_dashboard: dashboard, app: app2, app_version: app2.version, query_url: app2.base_url
    window2.save!

    return dashboard
  end

end
