require 'test_helper'

class WindowTest < ActiveSupport::TestCase

  test "should not save window without an user dashboard" do
    app = App.new name:'Fake App'
    window = Window.new app: app
    assert_not window.save
  end
  
  test "should not save window without an app" do
    user = User.new name:'Fake User', email:'fake@user.com', password:'123456'
    dashboard = UserDashboard.new user: user, description:'Fake Dashboard'
    window = Window.new user_dashboard: dashboard
    assert_not window.save
  end
  
  test "should save window with app and user dashboard" do
    user = User.new name:'Fake User', email:'fake@user.com', password:'123456'
    dashboard = UserDashboard.new user: user, description:'Fake Dashboard'
    app = App.new name:'Fake App'
    window = Window.new user_dashboard: dashboard, app: app
    assert window.save
  end
  
end
