require 'test_helper'

class UserDashboardTest < ActiveSupport::TestCase
  
  test "should not save user dashboard without an existing user" do
    dashboard = UserDashboard.new description:'No User!'
    assert_not dashboard.save
  end
  
  test "should not save user dashboard without a description" do
    user = User.new name:'Fake', email:'fake@user.com', password:'123456'
    dashboard = UserDashboard.new user: user
    assert_not dashboard.save
  end
  
  test "should save user dashboard with description and user" do
    user = User.new name:'Fake', email:'fake@user.com', password:'123456'
    dashboard = UserDashboard.new user: user, description:'All Ok'
    assert dashboard.save
  end
  
end
