# README

## Install dependencies:
```bash
bundle install
```
## Database creation:
```bash
rake db:create
rake db:migrate
rake db:seed
```

## Run tests:
```bash
rails test
```

## Run system tests:

System tests require capybara-webkit, which depends on Qt5 libraries to compile. So, do:

```bash
sudo apt-get update
sudo apt-get install qt5-default libqt5webkit5-dev gstreamer1.0-plugins-base gstreamer1.0-tools gstreamer1.0-x
```

Uncomment the following line from Gemfile:

```bash
#gem 'capybara-webkit'
```

Install and update (important!) the gem. It will take a while:

```bash
bundle install
bundle update
```

Everything ok, just run:

```bash
rake test:system
```

## If you want to test our tools with full access to all features use this account:

```bash
email: 'teste@teste.com'
password: '0000'
```

This account have developer privileges. With it, one can invite another developers.

## Sending general push notifications to all users

1. Log in with an account with developer privileges.

2. Navigate to http://localhost:3000/push-notification.

3. Fill in the form and press Send. The informed URL must be embeddable.

This done, when running on localhost or a SSL-secured server, will trigger a push notification.

A fallback for when these conditions are not satisfied is provided: the page reaches the server each
30s for new notifications.

Generated notifications can be found in Notification > Last 24h from SampaDash.

## Simulating Infocity notifications

To simulate capability-based requested notifications from Infocity:

1. Go to Notifictaion > New Notification Request and make a request.

Please inform _Show In Dashboard_ and create a window in the upcoming page.

2. Capture the generated token in the server output:

```bash
...
  SQL (8.4ms)  INSERT INTO "notification_tokens" ("user_id", "token", "state", "title", 
"created_at", "updated_at") VALUES (?, ?, ?, ?, ?, ?)  [["user_id", 8], ["token",
"ceffb074e4600525661c995f78ef6aa3f7a962f560203ea449249ebc81007103"], ["state", "1"], ["title", 
"Test"], ["created_at", "2017-12-01 17:35:30.541894"], ["updated_at", "2017-12-01 17:35:30.541894"]]
...
```

3. Trigger the notifications as if you were Infocity:

```bash
curl -XGET http://localhost:3000/push-notification-send-requested?token=ceffb074e4600525661c995f78ef6aa3f7a962f560203ea449249ebc81007103
```

Infocity notifications can be found in Notification > Last Notifications.
