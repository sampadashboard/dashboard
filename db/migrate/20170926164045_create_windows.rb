class CreateWindows < ActiveRecord::Migration[5.1]
  def change
    create_table :windows do |t|
      t.integer :col
      t.integer :row
      t.integer :size_x
      t.integer :size_y
      t.string :query_url
      t.string :app_version
      t.belongs_to :user_dashboard, foreign_key: true
      t.belongs_to :app, foreign_key: true

      t.timestamps
    end
  end
end
