class CreatePushNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :push_notifications do |t|
      t.string :message
      t.string :title
      t.string :url
      t.string :dev_token
      t.timestamps
    end
  end
end
