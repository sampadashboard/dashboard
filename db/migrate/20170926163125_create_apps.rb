class CreateApps < ActiveRecord::Migration[5.1]
  def change
    create_table :apps do |t|
      t.string :name
      t.string :version
      t.text :description
      t.string :base_url
      t.string :grid_size
      t.text :parameters
      t.integer :dev_team_id
      t.timestamps
    end
  end
end
