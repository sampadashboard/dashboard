class CreateNotificationTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :notification_tokens do |t|
      t.integer :user_id
      t.string :token, limit: 64
      t.string :state, limit: 1
      t.string :title
      t.string :query_url
      t.timestamps
    end
    add_index :notification_tokens, :token, unique: true
  end
end
