class UserFirebaseToken < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :firebase_token, :text
  end
end
