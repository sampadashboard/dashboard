class CreateNotificationApps < ActiveRecord::Migration[5.1]
  def change
    create_table :notification_apps do |t|
      t.integer :notification_token_id
      t.integer :app_id
      t.string :query_url
      t.timestamps
    end
    remove_column :notification_tokens, :query_url
  end
end
