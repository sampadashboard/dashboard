class CreateDevTeamsUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :dev_teams_users do |t|
      t.integer :dev_team_id
      t.integer :user_id

      t.timestamps
    end
    add_index :dev_teams_users, [:dev_team_id, :user_id]
  end
end
