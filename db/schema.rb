# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171128205655) do

  create_table "apps", force: :cascade do |t|
    t.string "name"
    t.string "version"
    t.text "description"
    t.string "base_url"
    t.string "grid_size"
    t.text "parameters"
    t.integer "dev_team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dev_teams", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dev_teams_users", force: :cascade do |t|
    t.integer "dev_team_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dev_team_id", "user_id"], name: "index_dev_teams_users_on_dev_team_id_and_user_id"
  end

  create_table "notification_apps", force: :cascade do |t|
    t.integer "notification_token_id"
    t.integer "app_id"
    t.string "query_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notification_tokens", force: :cascade do |t|
    t.integer "user_id"
    t.string "token", limit: 64
    t.string "state", limit: 1
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_notification_tokens_on_token", unique: true
  end

  create_table "push_notifications", force: :cascade do |t|
    t.string "message"
    t.string "title"
    t.string "url"
    t.string "dev_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tokens", force: :cascade do |t|
    t.string "token"
    t.string "email"
    t.string "invited_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_dashboards", force: :cascade do |t|
    t.integer "user_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password"
    t.integer "usertype"
    t.string "provider"
    t.string "uid"
    t.string "oauth_token"
    t.string "fone"
    t.datetime "oauth_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "firebase_token"
  end

  create_table "windows", force: :cascade do |t|
    t.integer "col"
    t.integer "row"
    t.integer "size_x"
    t.integer "size_y"
    t.string "query_url"
    t.string "app_version"
    t.integer "user_dashboard_id"
    t.integer "app_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app_id"], name: "index_windows_on_app_id"
    t.index ["user_dashboard_id"], name: "index_windows_on_user_dashboard_id"
  end

end
