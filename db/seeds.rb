# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
more_users = [
  {
    name: 'Felipe Caetano',
    email: 'felipecaetanosilva@hotmail.com',
    password: '012345',
    usertype: 1,
    fone: "974693954"
  },
  {
    name: 'Éderson Cássio',
    email: 'ederknopfler@hotmail.com',
    password: '012345',
    usertype: 1,
    fone: "974693954"
  },
  {
    name: 'Julio Ueda',
    email: 'julio.ueda@usp.br',
    password: '012345',
    usertype: 1,
    fone: "974693954"
  },
  {
    name: 'Camila Naomi',
    email: 'naomicamila@hotmail.com',
    password: '012345',
    usertype: 1,
    fone: "974693954"
  },
  {
    name: 'Marcos Vinicius',
    email: 'marcos_cadastre_seu_email@hotmail.com',
    password: '012345',
    usertype: 1,
    fone: "974693954"
  },
  {

    name: 'teste',
    email: 'teste@teste.com',
    password: '0000',
    usertype: 1,
    fone: "999999999"

  }
]

more_dev_teams =
  {
    name: 'SAMPADASH Team',
    description: 'We are the developers of this Dashboard'
  }

more_apps = [
  {
    name: 'Clock',
    version: '1.0',
    description: 'Show a clock',
    parameters: {},
    base_url: 'https://get-examples.herokuapp.com/clock/index',
    grid_size: '2x1'
  },
  {
    name: 'Graph',
    version: '1.0',
    description: 'Show a Graph',
    parameters: {},
    base_url: 'https://get-examples.herokuapp.com/graph/index',
    grid_size: '3x2'
  },
  {
    name: 'Temperature',
    version: '1.0',
    description: 'Show a termometer image',
    parameters: {},
    base_url: 'https://get-examples.herokuapp.com/temperature/index',
    grid_size: '1x2'
  },
  {
    name: 'Youtube',
    version: '1.0',
    description: 'Put a Youtube video in your widget',
    parameters: {video_url: 'text_field', comment: 'text_area', rate: ['select', [['awesome', 3], ['good', 2], ['shit', 1]]]},
    base_url: 'https://get-youtube.herokuapp.com',
    grid_size: '4x3'
  },
  {
    name: 'Kibana Map',
    version: '1.0',
    description: 'Sample Kibana-generated map',
    parameters: {},
    base_url: "https://demo.elastic.co/app/kibana#/visualize/edit/d1726930-0a7f-11e7-8b04-eb22a5669f27?embed=true&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-24h,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'*')),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(),schema:metric,type:count),(enabled:!t,id:'2',params:(autoPrecision:!t,field:auditd.log.geoip.location,precision:2,useGeocentroid:!t),schema:segment,type:geohash_grid)),listeners:(),params:(addTooltip:!t,heatBlur:15,heatMaxZoom:16,heatMinOpacity:0.1,heatNormalizeData:!t,heatRadius:25,isDesaturated:!t,legendPosition:bottomright,mapCenter:!(15,5),mapType:'Scaled+Circle+Markers',mapZoom:2,wms:(enabled:!f,options:(attribution:'Maps+provided+by+USGS',format:image%2Fpng,layers:'0',styles:'',transparent:!t,version:'1.3.0'),url:'https:%2F%2Fbasemap.nationalmap.gov%2Farcgis%2Fservices%2FUSGSTopo%2FMapServer%2FWMSServer')),title:'Audit+Event+Address+Geo+Location',type:tile_map))",
    grid_size: '5x3'
  }
]

more_requested_notifications = [
  {
    user_id: 6,
    token: 'abc',
    state: NotificationToken::RequestedState,
    title: 'Caos no trânsito!',
    #query_url: 'https://www.youtube.com/embed/yb7g6p6tf-U',
  },
  {
    user_id: 6,
    token: 'def',
    state: NotificationToken::RequestedState,
    title: 'Enchente em SP',
    #query_url: 'https://www.youtube.com/embed/I4f7c6LOGEE',
  }
]

more_general_notifications = [
  {
    title: 'Parque Ibirapuera',
    message: 'Amostra de notificação',
    url: 'https://www.youtube.com/embed/LrSxD6hx5kM'
  }
]

dev_team = DevTeam.create more_dev_teams
more_users.each do |user|
  dev_team.users.create user
end


more_apps.each do |app|
  dev_team.apps.create app
end

NotificationToken.create more_requested_notifications
PushNotification.create more_general_notifications
