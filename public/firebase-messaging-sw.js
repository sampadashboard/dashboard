importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

firebase.initializeApp({
  'messagingSenderId': '341587114548'
});

var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  var notificationTitle = payload.data.title;
  var notificationOptions = {
    body: payload.data.message,
    icon: '/sampa-dashboard-logo.png'
  };

  clients.matchAll({includeUncontrolled: true, type: 'window'})
    .then(function (clients) {
      for (var i in clients)
        clients[i].postMessage({
          title: payload.data.title,
          url: payload.data.url
        });
    });
    
  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
